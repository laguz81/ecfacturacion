[![Quarkus](http://ecticsoft.com/img/logofenix.png)](http://ecticsoft/)

## RELEASE V0.0.1


# ECFACTURACION - Facturación Electrónica Ecuador

ECFACTURACION Es un Sistema SASS y también una aplicación que permite realizar Facturación ElectrónICA

* **Servicio SASS**: 
Permite que otras aplicaciones se conecten a través de APIS de Servicios Autenticados y autorizados para recibir un documento y proceder con la generación del documento electrónico, como facturas, notas de crédito, notas de débito, guías de remisión y comprobantes de retención normales y ATM
* **Aplicación Web Facturación Electrónica**:
Mediante la aplicación Web permite tener un sistema de Facturación


_Todo bajo un mismo sistema._

## Empiece

* [Documentación](http://ecticsoft.com)


## Derechos

* Realizado por Ing Luis Arturo Guzman Lopez
* [ecticsoft.com](http://ecticsoft.com) ©copyright 2019 -  Todos los derechos reservados.

