package com.ecticsoft;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.containsString;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class quickstartTest {
 
    @Test
    public void testHelloEndpoint() {
        given()
            .when()
            .get("/hello/luis")
            .then()
                .statusCode(200)
                .body(is("hello luis"));
    }

    @Test
    public void testHelloResponseJson() {
        //JsonObject helloJson = Json.createObjectBuilder().add("name", "luis").build();      
      
        given()
            .when()
            .get("/hello/saludos/luis")
            .then()
                .statusCode(200)
                .body(containsString("name"), containsString("luis"));
    }

}