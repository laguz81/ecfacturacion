package com.ecticsoft.hello;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.jwt.JsonWebToken;

import java.security.Principal;

@RequestScoped
@Path("hello")
public class HelloResource {
    @Inject
    Principal principal;

    @Inject
    JsonWebToken token;

    @Inject
    HelloService helloservice;

    @GET
    @PermitAll
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "principal "+ principal.getName() + " groups: " + token.getGroups();
    }

    @GET
    @RolesAllowed("user-ec")
    @Path("user/role")
    public String luisOnly(){
        return "principal "+ this.principal.getName() + " groups: " + token.getGroups();

    }

    @GET
    @PermitAll
    @Produces(MediaType.TEXT_PLAIN)
    @Path("{name}")
    public String hello(@PathParam("name") String name) {
        return "hello " + helloservice.hello(name);
    }

    @GET
    @PermitAll
    @Produces(MediaType.APPLICATION_JSON)
    @Path("saludos/{name}")
    public Hello saludos(@PathParam("name") String name) {
        return new Hello(name);
    }
}