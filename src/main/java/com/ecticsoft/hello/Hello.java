package com.ecticsoft.hello;

import lombok.Data;

/**
 * Hello
 */
@Data
public class Hello {

    public String name;

    public Hello(String name){
        this.name = name;    
    }
}