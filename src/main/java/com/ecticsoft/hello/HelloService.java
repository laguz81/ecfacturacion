package com.ecticsoft.hello;

import javax.enterprise.context.ApplicationScoped;

/**
 * HelloService
 */
@ApplicationScoped
public class HelloService {

    public String hello(String name){
        return name;
    }
}